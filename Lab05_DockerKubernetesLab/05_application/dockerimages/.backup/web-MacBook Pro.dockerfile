ARG VERSION=latest
FROM node

# Create app directory
WORKDIR .
RUN npm install




LABEL app="cldinf-web"
EXPOSE 8080/tcp

ENTRYPOINT ["nginx", "-b"]

ENV MODULE_NAME=*
ENV GROUP_NAME=*
ENV API_HOST=*
ENV API_PORT=8080
