conf t
feature pim
ip pim rp-address 192.168.0.100

int lo0
ip pim sparse-mode
exit
int lo1
ip pim sparse-mode
exit
int e1/1
ip pim sparse-mode
exit
int e1/2
ip pim sparse-mode
exit

int e1/3
switchport access vlan 140
exit

vlan 140
    vn-segment 50140
    exit

feature nv overlay
feature vn-segment-vlan-based

interface nve1
    no shutdown
    source-interface lo1
    member vni 50140
        mcast-group 239.0.0.140
        exit
exit

----------------


feature bgp
router bgp 65000

  router-id 192.168.0.30

  neighbor 10.0.0.30 remote-as 65000
  address-family ipv4 unicast
  exit

  neighbor 10.0.128.14 remote-as 65000
  address-family ipv4 unicast
  exit

exit
