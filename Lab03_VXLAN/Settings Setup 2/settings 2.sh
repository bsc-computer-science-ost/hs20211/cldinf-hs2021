spine 1

feature bgp

router bgp 65000
    neighbor 10.0.0.21 remote-as 65000
        address-family ipv4 unicast
            route-reflector-client
            send-community both
            exit
        exit
    neighbor 10.0.0.25 remote-as 65000
        address-family ipv4 unicast
            route-reflector-client
            send-community both
            exit
        exit
    neighbor 10.0.0.29 remote-as 65000
        address-family ipv4 unicast
            route-reflector-client
            send-community both
            exit
        exit
    neighbor 10.0.0.33 remote-as 65000
        address-family ipv4 unicast
            route-reflector-client
            send-community both
            exit
        exit
    exit


----------------------------
spine 2

feature bgp

router bgp 65000
    route-reflector-client
    neighbor 10.0.128.5 remote-as 65000
        address-family ipv4 unicast
            route-reflector-client
            send-community both
            exit
        exit
    neighbor 10.0.128.9 remote-as 65000
        address-family ipv4 unicast
            route-reflector-client
            send-community both
            exit
        exit
    neighbor 10.0.128.13 remote-as 65000
        address-family ipv4 unicast
            route-reflector-client
            send-community both
            exit
        exit
    neighbor 10.0.128.17 remote-as 65000
        address-family ipv4 unicast
            route-reflector-client
            send-community both
            exit
        exit
    exit


---------------------
VLAN 999
--------

vlan 999
  vn-segment 50999

interface vlan999
  no shutdown
  ip forward

-----------------
3. vrf

nv overlay evpn

router bgp 65000
    vrf context Tenant-1
        vni 65000
        rd auto
        address-family ipv4 unicast
            route-target both auto
            route-target both auto evpn

---------
4. SVIs
feature interface-vlan

interface vlan999
    no shutdown
    vrf member Tenant-1
    no ip redirects
    fabric forwarding mode anycast-gateway


feature interface-vlan
fabric forwarding anycast-gateway-mac 0001.0001.0001

interface vlan140
    no shutdown
    vrf member Tenant-1
    no ip redirects
    ip address 172.21.140.1/24
    fabric forwarding mode anycast-gateway
exit

interface vlan141
    no shutdown
    vrf member Tenant-1
    no ip redirects
    ip address 172.21.141.1/24
    fabric forwarding mode anycast-gateway
exit

---------------

5. configure /adjust nve

interface nve1
    host-reachability protocol bgp
    member vni 50140
        mcast-group 239.0.0.140
        exit
    member vni 50141
        mcast-group 239.0.0.141
        exit
    member vni 50999
        mcast-group 239.0.0.99
        exit
    exit


-----------------
6. configure ebgp evpn

router bgp 50000
    address-family l2vpn evpn
    retain route-target all
    send-community both
